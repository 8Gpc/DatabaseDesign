package com.tianwu.databasedesign;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class SettingActivity extends ActionBarActivity {

    private TextView v_id;
    private TextView v_name;
    private TextView v_funt;
    private TextView v_local;

    private ListView listView;

    private ImageButton add;
    private ImageButton edit;
    private ImageButton back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        v_id = (TextView)findViewById(R.id.editTextRoomId);
        v_name = (TextView)findViewById(R.id.editTextRoomName);
        v_funt = (TextView)findViewById(R.id.editTextRoomFunt);
        v_local = (TextView)findViewById(R.id.editTextRoomLocal);
        initView();


        add = (ImageButton)findViewById(R.id.imageButtonAddRoom);
        add.setEnabled(false);

        listView = (ListView)findViewById(R.id.listViewRoom);

        final ArrayList<Lab> labs = Sql.getAllLab(this);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setData(labs.get(position));
            }
        });

        if(labs != null) {
            ArrayList<String> list = new ArrayList<>();
            for (Lab l : labs)
                list.add(l.name);
            listView.setAdapter(new ListViewAdapte(listView.getContext(), list));
        }

    }
    public void addRoom(View view){
        if(Sql.addRoom(this,getData())){
            Toast.makeText(this,"add room succeefuly.",Toast.LENGTH_LONG).show();
            final ArrayList<Lab> labs = Sql.getAllLab(this);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    setData(labs.get(position));
                }
            });

            ArrayList<String> list = new ArrayList<>();
            for(Lab l : labs)
                list.add(l.name);
            listView.setAdapter(new ListViewAdapte(listView.getContext(),list));
        }else {
            Toast.makeText(this,"add room error.",Toast.LENGTH_LONG).show();
        }
        initView();
    }
    public void editRoom(View view){
        setData(new Lab());
        v_id.setEnabled(true);
        v_name.setEnabled(true);
        v_funt.setEnabled(true);
        v_local.setEnabled(true);
        add.setEnabled(true);
    }
    public void bace2main(View view){
        this.finish();
    }
    private void initView(){
        v_id.setEnabled(false);
        v_name.setEnabled(false);
        v_funt.setEnabled(false);
        v_local.setEnabled(false);
    }
    private void setData(Lab lab){
        v_id.setText(lab.id);
        v_name.setText(lab.name);
        v_funt.setText(lab.functionary);
        v_local.setText(lab.location);
    }
    private Lab getData(){
        return new Lab(
                v_id.getText().toString(),
                v_name.getText().toString(),
                v_funt.getText().toString(),
                v_local.getText().toString()
        );
    }
}
