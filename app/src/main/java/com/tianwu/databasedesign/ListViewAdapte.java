package com.tianwu.databasedesign;


import android.content.Context;

import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Tianwu on 2016/6/28.
 */
public class ListViewAdapte extends BaseAdapter {

    private ArrayList<String> items;
    private Context context;
    public ListViewAdapte(Context context, ArrayList<String> items){
        this.context = context;
        if(items == null)
            this.items = new ArrayList<>();
        else
            this.items = items;
    }
    @Override
    public int getCount() {
        return items.size();
    }
    @Override
    public Object getItem(int position){
        return items.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {

        TextView view = (TextView) convertView;
        if(view == null){
            view = new TextView(context);
        }
        view.setText(items.get(position));

        return view;
    }
}
