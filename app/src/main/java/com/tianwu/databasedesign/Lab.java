package com.tianwu.databasedesign;

/**
 * Created by Tianwu on 2016/6/28.
 */
public class Lab {

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String FUNCTIONARY = "functionary";//负责人
    public static final String LOCATION = "location";//位置

    public String id;
    public String name;
    public String functionary;
    public String location;

    public Lab(){
        id = "";
        name = "";
        functionary = "";
        location = "";
    }
    public Lab(String id,String name,String functionary,String location){
        this.id = id;
        this.name = name;
        this.functionary = functionary;
        this.location = location;
    }
}
