package com.tianwu.databasedesign;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    public static final int CALL_INDEX = 200;


    private ListView showList;

    private ArrayList<Equipment> equipments;

    private boolean returnFromSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showList = (ListView)findViewById(R.id.listView);


        returnFromSearch = false;

        /*
        final ArrayList<Equipment> equipments = Sql.getAllEquipment(this);

        showList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent addActivity = new Intent(showList.getContext(),AddActivity.class);
                addActivity.putExtra(AddActivity.FUNCTION,AddActivity.SHOW_ITEM);
                addActivity.putExtra(AddActivity.ITEM_ID,equipments.get(position).id);
                startActivity(addActivity);
            }
        });
        if(equipments != null) {
            ArrayList<String> list = new ArrayList<>();
            for (Equipment e : equipments)
                list.add(e.name);
            showList.setAdapter(new ListViewAdapte(showList.getContext(), list));
        }
        */
        showList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent addActivity = new Intent(showList.getContext(),AddActivity.class);
                addActivity.putExtra(AddActivity.FUNCTION,AddActivity.SHOW_ITEM);
                addActivity.putExtra(AddActivity.ITEM_ID,equipments.get(position).id);
                startActivity(addActivity);
            }
        });

    }
    @Override
    protected void onResume(){
        super.onResume();

        if(!returnFromSearch) {
            equipments = Sql.getAllEquipment(this);
            returnFromSearch = false;
        }
        if(equipments != null) {
            ArrayList<String> list = new ArrayList<>();
            for (Equipment e : equipments)
                list.add(e.name);
            showList.setAdapter(new ListViewAdapte(showList.getContext(), list));
        }

    }

    public void search(View view){
        Toast.makeText(this,"search...", Toast.LENGTH_LONG).show();
        Intent searchActivity = new Intent(this,SearchActivity.class);
        startActivityForResult(searchActivity,CALL_INDEX);
    }
    public void add(View view){

        Toast.makeText(this,"add...",Toast.LENGTH_LONG).show();
        Intent addActivity = new Intent(this,AddActivity.class);
        addActivity.putExtra(AddActivity.FUNCTION,AddActivity.ADD_ITEM);
        startActivity(addActivity);
    }
    public void setting(View view){
        Toast.makeText(this,"setting...",Toast.LENGTH_LONG).show();
        Intent settingActivity = new Intent(this,SettingActivity.class);
        startActivity(settingActivity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent returnIntent) {
        if(resultCode == RESULT_OK && requestCode == CALL_INDEX){

            returnFromSearch = true;

            String searchId = returnIntent.getStringExtra(SearchActivity.SEARCH_ID);
            String searchName = returnIntent.getStringExtra(SearchActivity.SEARCH_NAME);
            String searchRoom = returnIntent.getStringExtra(SearchActivity.SEARCH_FUNCTION);

            equipments = Sql.query(this,searchId,searchName,searchRoom);
            //Toast.makeText(this,"search " + searchId + searchName + searchRoom + result.size(),Toast.LENGTH_LONG).show();
            ArrayList<String> list = new ArrayList<>();
            for(Equipment e : equipments)
                list.add(e.name);
            showList.setAdapter(new ListViewAdapte(showList.getContext(),list));

        }
    }
}
