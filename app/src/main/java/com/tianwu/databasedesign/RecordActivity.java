package com.tianwu.databasedesign;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class RecordActivity extends AppCompatActivity {

    private TextView v_id;
    private TextView v_name;
    private TextView v_date;
    private TextView v_cost;
    private TextView v_project;
    private TextView v_others;

    private ListView listView;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        id = getIntent().getStringExtra(AddActivity.ITEM_ID);
        final ArrayList<Record> records = Sql.getAllRecord(this,id);

        v_id = (TextView)findViewById(R.id.editTextRoomId);
        v_name = (TextView)findViewById(R.id.editTextRoomName);
        v_date = (TextView)findViewById(R.id.editTextRoomFunt);
        v_project = (TextView)findViewById(R.id.editTextRoomLocal);
        v_cost = (TextView)findViewById(R.id.editTextRcost);
        initView();

        listView = (ListView)findViewById(R.id.listViewRecord);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Record record = records.get(position);
                setData(record);
            }
        });

        if(records != null) {
            ArrayList<String> list = new ArrayList<>();
            for (Record r : records)
                list.add(r.date);
            listView.setAdapter(new ListViewAdapte(listView.getContext(), list));
        }
    }
    private void initView(){
        v_id.setEnabled(false);
        v_id.setText(id);
        v_date.setEnabled(false);
        v_cost.setEnabled(false);
        v_project.setEnabled(false);
        v_name.setEnabled(false);
        v_name.setText(Sql.getEqNameById(this,id));
    }
    private void setData(Record record){
        //v_id.setText(record.id);
        //v_name.setText(Sql.getEqNameById(this,id));
        v_date.setText(record.date);
        v_cost.setText(Float.toString(record.cost));
        v_project.setText(record.project);
    }
    private Record getData(){
        return new Record(
                v_id.getText().toString(),
                v_date.getText().toString(),
                Float.parseFloat(v_cost.getText().toString()),
                v_project.getText().toString(),
                //v_others.getText().toString()
                "..."
        );
    }
    public void addRecord(View view){
        Toast.makeText(this,"add record...",Toast.LENGTH_SHORT).show();
        if(Sql.addRecord(this,getData())){
            Toast.makeText(this,"add record succeefuly.",Toast.LENGTH_SHORT).show();
            final ArrayList<Record> records = Sql.getAllRecord(this,id);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Record record = records.get(position);
                    setData(record);
                }
            });
            if(records != null) {
                ArrayList<String> list = new ArrayList<>();
                for (Record r : records)
                    list.add(r.date);
                listView.setAdapter(new ListViewAdapte(listView.getContext(), list));
            }
        }else {
            Toast.makeText(this,"add record error.",Toast.LENGTH_LONG).show();
        }

    }
    public void editRecord(View view){
        Record r = new Record();
        //r.id = id;
        setData(r);
        v_date.setEnabled(true);
        v_cost.setEnabled(true);
        v_project.setEnabled(true);
    }
    public void back2Show(View view){
        this.finish();
    }
}
