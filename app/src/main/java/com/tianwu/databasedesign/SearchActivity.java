package com.tianwu.databasedesign;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class SearchActivity extends AppCompatActivity {

    public static final String SEARCH_ID = "id";
    public static final String SEARCH_NAME = "name";
    public static final String SEARCH_FUNCTION = "function";//所属实验室.

    private TextView viewID;
    private TextView viewName;
    private TextView viewFunc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        viewID = (TextView)findViewById(R.id.editTextSId);
        viewName = (TextView)findViewById(R.id.editTextSName);
        viewFunc = (TextView)findViewById(R.id.editTextSRoom);
    }
    public void toSearch(View view){
        Intent backResult = new Intent(this,MainActivity.class);
        backResult.putExtra(SEARCH_ID,viewID.getText().toString());
        backResult.putExtra(SEARCH_NAME,viewName.getText().toString());
        backResult.putExtra(SEARCH_FUNCTION,viewFunc.getText().toString());
        setResult(RESULT_OK,backResult);
        this.finish();
    }
    public void back(View view){
        setResult(RESULT_CANCELED);
        this.finish();
    }
}
