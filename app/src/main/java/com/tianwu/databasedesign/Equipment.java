package com.tianwu.databasedesign;

import java.util.Date;

/**
 * Created by Tianwu on 2016/6/28.
 */
public class Equipment {

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String KIND = "kind";//类型
    public static final String CHAMBER = "chamber";//使用单位
    public static final String DATE = "date";
    public static final String FACTORY = "factory";//出产厂家
    public static final String EXPENDITURE = "expenditure";//经费

    public String id;
    public String name;
    public String kind;
    public String chamber;
    public String date;
    public String factory;
    public float expenditure;

    public Equipment(){

    }

    public Equipment(String id,String name,String kind,String chamber,String date,String factory,float expenditure){
        this.id = id;
        this.name = name;
        this.kind = kind;
        this.chamber = chamber;
        this.date = date;
        this.factory = factory;
        this.expenditure = expenditure;
    }

}
