package com.tianwu.databasedesign;

/**
 * Created by Tianwu on 2016/6/29.
 */
public class Record {

    public static final String ID = "id";
    public static final String DATE = "date";
    public static final String COST = "cost";
    public static final String PROJECT = "project";
    public static final String OTHERS = "others";

    public String id;
    public String date;
    public float cost;
    public String project;
    public String others;

    public Record (){
        id = "";
        date = "";
        cost = 0;
        project = "";
        others = "";

    }
    public Record(String id,String date,float cost,String project,String other){
        this.id = id;
        this.date = date;
        this.cost = cost;
        this.project = project;
        this.others = other;
    }
}
