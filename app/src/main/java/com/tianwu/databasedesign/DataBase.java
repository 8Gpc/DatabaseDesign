package com.tianwu.databasedesign;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Tianwu on 2016/6/28.
 */
public class DataBase extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "database_design.db";

    public static final String TABLE_EQU_NAME = "equipments";
    public static final String CREATE_TABLE_EQU =
            "CREATE TABLE " + TABLE_EQU_NAME + " ("+
                    Equipment.ID  +  " TEXT PRIMARY KEY," +
                    Equipment.NAME + " TEXT NOT NULL," +
                    Equipment.KIND + " TEXT," +
                    Equipment.CHAMBER + " TEXT," +
                    Equipment.DATE + " DATE," +
                    Equipment.FACTORY + " TEXT," +
                    Equipment.EXPENDITURE + " REAL" +
                    ");";
    public static final String DELETE_TABLE_EQU =
            "DROP TABLE IF EXISTS " + TABLE_EQU_NAME;

    public static final String TABLE_LAB_NAME = "lab";
    public static final String CREATE_TABLE_LAB =
            "CREATE TABLE " + TABLE_LAB_NAME + " ("+
                    Lab.ID + " TEXT PRIMARY KEY," +
                    Lab.NAME + " TEXT UNIQUE NOT NULL," +
                    Lab.FUNCTIONARY + " TEXT," +
                    Lab.LOCATION + " TEXT" +
                    ");";
    public static final String DELETE_TABLE_LAB =
            "DROP TABLE IF EXISTS " + TABLE_LAB_NAME;

    public static final String TABLE_RECORD_NAME = "record";
    public static final String CREATE_TABLE_RECORD =
            "CREATE TABLE " + TABLE_RECORD_NAME + " ("+
                    Record.ID + " TEXT," +
                    Record.DATE + " DATE," +
                    Record.COST + " REAL," +
                    Record.PROJECT + " TEXT," +
                    Record.OTHERS + " TEXT" +
                    ");";
    public static final String DELETE_TABLE_RECORD =
            "DROP TABLE IF EXISTS " + TABLE_RECORD_NAME;


    public DataBase(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_EQU);
        db.execSQL(CREATE_TABLE_LAB);
        db.execSQL(CREATE_TABLE_RECORD);
    }
    @Override
    public void onOpen(SQLiteDatabase db){

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(DELETE_TABLE_EQU);
        db.execSQL(DELETE_TABLE_LAB);
        db.execSQL(DELETE_TABLE_RECORD);
        onCreate(db);
    }
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
