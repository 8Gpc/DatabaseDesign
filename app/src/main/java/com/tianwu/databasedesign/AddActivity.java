package com.tianwu.databasedesign;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

public class AddActivity extends AppCompatActivity {

    public static final String FUNCTION = "function";
    public static final String ITEM_ID = "itemId";
    public static final int ADD_ITEM = 1;
    public static final int SHOW_ITEM = 2;

    private TextView viewID;
    private TextView viewName;
    private TextView viewKind;
    private TextView viewChamber;
    private TextView viewDate;
    private TextView viewFactory;
    private TextView viewExpenditure;
    private TextView viewFunctioary;
    private TextView viewRecode;

    private ImageButton saveButton;
    private ImageButton editButton;
    private ImageButton deleteButton;

    private TextView showLog;

    private int functionIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        viewID = (TextView)findViewById(R.id.editTextID);
        viewName = (TextView)findViewById(R.id.editTextName);
        viewKind = (TextView)findViewById(R.id.editTextKind);
        viewChamber = (TextView)findViewById(R.id.editTextChamber);
        viewDate = (TextView)findViewById(R.id.editTextDate);
        viewFactory = (TextView)findViewById(R.id.editTextFactory);
        viewExpenditure = (TextView)findViewById(R.id.editTextExpenditure);
        viewFunctioary = (TextView)findViewById(R.id.editTextFunctionary);
        viewRecode = (TextView)findViewById(R.id.editTextRecord);
        initTextView();

        saveButton = (ImageButton)findViewById(R.id.imageButtonSave);
        editButton = (ImageButton)findViewById(R.id.imageButtonEdit);
        deleteButton = (ImageButton)findViewById(R.id.imageButtonDelete);

        showLog = (TextView)findViewById(R.id.log);

        functionIndex = getIntent().getIntExtra(FUNCTION,0);
        switch (functionIndex){
            case ADD_ITEM:
                initAdd();
                break;
            case SHOW_ITEM:
                initShow();
                break;
            default:
                initDefault();
                break;
        }

    }
    private void initTextView(){
        viewID.setEnabled(false);
        viewName.setEnabled(false);
        viewKind.setEnabled(false);
        viewChamber.setEnabled(false);
        viewDate.setEnabled(false);
        viewFactory.setEnabled(false);
        viewExpenditure.setEnabled(false);
        viewFunctioary.setEnabled(false);
        viewRecode.setEnabled(false);
    }
    private void setAbleTextView(){
        viewID.setEnabled(true);
        viewName.setEnabled(true);
        viewKind.setEnabled(true);
        viewChamber.setEnabled(true);
        viewDate.setEnabled(true);
        viewFactory.setEnabled(true);
        viewExpenditure.setEnabled(true);
        //viewFunctioary.setEnabled(true);
    }

    private void initDefault(){
        deleteButton.setClickable(false);
        editButton.setClickable(false);
        saveButton.setClickable(false);
        showLog.setText("Error.");
    }
    private void initAdd(){
        deleteButton.setClickable(false);
        editButton.setClickable(true);
        saveButton.setClickable(true);
        showLog.setText("edit item for add.");
    }
    private void initShow(){
        deleteButton.setClickable(false);
        editButton.setClickable(true);
        saveButton.setClickable(true);
        showLog.setText("show.");

        String equId = getIntent().getStringExtra(ITEM_ID);
        Toast.makeText(this,equId,Toast.LENGTH_LONG).show();
        Equipment equipment = Sql.getEquipmentById(this,equId);
        setData(equipment);
    }

    public void save(View view){
        Toast.makeText(this,"save...", Toast.LENGTH_LONG).show();
        switch (functionIndex){
            case ADD_ITEM:
                Equipment e = getData();
                if(e != null && Sql.addEquipment(this, e)) {
                    Toast.makeText(this, "Add new item succeedfuly.", Toast.LENGTH_LONG).show();
                    this.finish();
                }else {
                    showLog.setText("Add error, please edit item again.");
                }
                break;
            case SHOW_ITEM:
                break;
            default:
                break;
        }
    }
    public void edit(View view){
        Toast.makeText(this,"edit...", Toast.LENGTH_LONG).show();
        setAbleTextView();
        switch (functionIndex){
            case ADD_ITEM:

                break;
            case SHOW_ITEM:
                break;
            default:
                break;
        }
    }
    public void delete(View view){
        Toast.makeText(this,"delete...", Toast.LENGTH_LONG).show();
        switch (functionIndex){
            case ADD_ITEM:
                break;
            case SHOW_ITEM:
                if(Sql.deleteEquipmentById(this, getIntent().getStringExtra(ITEM_ID))){
                    Toast.makeText(this, "Delete item succeedfuly.", Toast.LENGTH_LONG).show();
                    this.finish();
                }else {
                    showLog.setText("Delete error.");
                }
                break;
            default:
                break;
        }
    }
    public void addRecode(View view){//add recode.
        Toast.makeText(this,"add record...", Toast.LENGTH_LONG).show();
        Intent recordActivity = new Intent(this,RecordActivity.class);
        recordActivity.putExtra(AddActivity.ITEM_ID,getIntent().getStringExtra(ITEM_ID));
        startActivity(recordActivity);
    }

    private void setData(Equipment e){
        Lab lab = Sql.getLabById(this,e.chamber);
        viewID.setText(e.id);
        viewName.setText(e.name);
        viewKind.setText(e.kind);
        viewChamber.setText(lab.name);
        viewDate.setText(e.date.toString());
        viewFactory.setText(e.factory);
        viewExpenditure.setText(Float.toString(e.expenditure));
        viewFunctioary.setText(lab.functionary);
    }
    private Equipment getData(){
        String room = viewChamber.getText().toString();
        if(Sql.getLabById(this,room) == null) {
            Toast.makeText(this, "no this room, please add it in setting.", Toast.LENGTH_LONG).show();
            return null;
        }else{
            Equipment equipment = new Equipment(
                    viewID.getText().toString(),
                    viewName.getText().toString(),
                    viewKind.getText().toString(),
                    viewChamber.getText().toString(),
                    viewDate.getText().toString(),
                    viewFactory.getText().toString(),
                    Float.parseFloat(viewExpenditure.getText().toString())
            );
            return equipment;
        }

    }
}
